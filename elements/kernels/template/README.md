# TEMPLATE

This is a template for the layout of a kernel.

- The README should contain a short description of the kernel variant
- `kernel.bst` should contain the build definition of the kernel
- `initramfs.bst` should contain the initramfs that pairs with this kernel
- `repo.bst` should contain the OSTree repo w/ this kernel's ref commited
