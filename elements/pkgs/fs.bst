kind: manual
description: |
  carbonOS base filesystem layout

  /
  ├── bin -> /usr/bin                 # Compat symlink for software that hardcodes /bin
  ├── lib -> /usr/lib                 # Compat symlink for software that hardcodes /lib
  ├── lib64 -> /usr/lib               # Compat symlink for software that hardcodes /lib64
  ├── sbin -> /usr/bin                # Compat symlink for software that hardcodes /sbin
  ├── boot                            # Mount point for bootloader config (ESP)
  ├── etc                             # Empty /etc to be populated by OSTree
  ├── home -> /var/home               # Compat symlink for home directory
  ├── sys                             # Mount point for sysfs
  ├── mnt                             # Mount point for temporary mounts
  ├── opt -> /var/opt                 # Place to install extra software
  ├── ostree -> /sysroot/ostree       # Symlink required by OSTree
  ├── sysroot                         # Mount point for the real system root
  ├── run                             # Mount point for the run tmpfs
  ├── proc                            # Mount point for procfs
  ├── tmp                             # Mount point for tmpfs
  ├── root -> /var/home/root          # Compat symlink for root's home directory
  └── usr
      ├── sbin -> /usr/bin            # Links /usr/sbin to /usr/bin
      └── local -> /var/usrlocal      # Allows /usr/local to be writeable

build-depends:
- buildsystems/base.bst

config:
  install-commands:
  - |
    cd %{install-root}
    ln -sv /usr/bin ./bin
    ln -sv /usr/bin ./sbin
    ln -sv /usr/lib ./lib
    ln -sv /usr/lib ./lib64
    mkdir -v ./boot
    mkdir -v ./etc
    ln -sv /var/home ./home
    mkdir -v ./sys
    mkdir -v ./mnt
    ln -sv /var/opt ./opt
    ln -sv /sysroot/ostree ./ostree
    mkdir -v ./sysroot
    mkdir -v ./run
    mkdir -v ./proc
    mkdir -v ./tmp
    ln -sv /var/home/root ./root
    mkdir -v ./usr
    ln -sv /var/usrlocal ./usr/local
    ln -sv /usr/bin ./usr/sbin
    chown -vhR 0:0 .
